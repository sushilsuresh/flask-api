#!/bin/bash

VIRTUALENV=$(which virtualenv)

set -euf -o pipefail

if [ "${VIRTUALENV}" == "" ]
then
    echo -e "\nPlease install virtualenv before running setup.sh\n"
    exit 1
fi

echo -e "\nSetting up virtual env\n"
virtualenv venv
source venv/bin/activate

echo -e "\nInstall necessary python modules\n"
pip install -r requirements.txt

echo -e "\n\t Running Tests.\n"
python -m unittest discover ./tests/ -v

if [ ! -f people.db ]
then
    echo -e "\nCreate sqlite3 database people.db\n"
    python -c "from app import db; db.create_all()"
fi

echo -e "\nExample curl commands to test REST API\n"
cat curl-commands.txt

echo -e "\nStarting the app by running - 'python app.py'\n"
echo -e "Press Ctrl-c to exit\n"

python app.py
