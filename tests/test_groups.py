import unittest
import json
from app import app, Users, Groups
from app import db


class GroupTest(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        db.drop_all()
        self.db = db.create_all()

    def tearDown(self):
        db.drop_all()

    def test_group_creation(self):

        group = {'group_name': 'test_group'}

        response = self.app.post('/groups',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(group))

        self.assertEqual('test_group', response.json['group_name'])
        self.assertEqual(200, response.status_code)

    def test_group_creation_with_duplicate_group_name(self):
        # Create the group we need to try to recreate
        group = Groups(group_name='test_group')
        db.session.add(group)
        db.session.commit()

        group = {'group_name': 'test_group'}

        response = self.app.post('/groups',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(group))

        self.assertEqual(409, response.status_code)

    def test_group_creation_with_malformed_json(self):

        group = {'my_group_name': 'test_group'}

        response = self.app.post('/groups',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(group))

        self.assertEqual(400, response.status_code)

    def test_group_creation_with_nonexisting_users(self):
        group = {
            'group_name': 'test_group',
            'users': ["user1"],
        }

        response = self.app.post('/groups',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(group))

        self.assertEqual(404, response.status_code)

    def test_group_creation_with_existing_user(self):
        # Create user "user1"
        user = Users(userid='user1',
                     first_name='user1_first',
                     last_name='user1_last')
        db.session.add(user)
        db.session.commit()

        # Create Test group
        group = {
            'group_name': 'test_group',
            'users': ["user1"],
        }

        response = self.app.post('/groups',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(group))

        self.assertEqual(200, response.status_code)

    def test_fetch_group_without_user_membership(self):
        # Create the group we need to fetch
        group = Groups(group_name='test_group')
        db.session.add(group)
        db.session.commit()

        response = self.app.get(
                '/groups/test_group',
                headers={"Content-Type": "application/json"})

        self.assertEqual('test_group', response.json['group_name'])
        self.assertEqual(200, response.status_code)

    def test_fetch_group_with_user_membership(self):
        # Create user user1
        user = Users(userid='user1',
                     first_name='user1_first',
                     last_name='user1_last')
        db.session.add(user)
        db.session.commit()

        # Create the group we need to fetch
        group = Groups(
                group_name='test_group',
                users=[user])
        db.session.add(group)
        db.session.commit()

        response = self.app.get(
                '/groups/test_group',
                headers={"Content-Type": "application/json"})

        self.assertEqual('test_group', response.json['group_name'])
        self.assertEqual('user1', response.json['users'][0])
        self.assertEqual(200, response.status_code)

    def test_group_deletion_without_user_membership(self):
        # Create the group we need to delete
        group = Groups(group_name='test_group')
        db.session.add(group)
        db.session.commit()

        response = self.app.delete(
                '/groups/test_group',
                headers={"Content-Type": "application/json"})

        self.assertEqual(204, response.status_code)

    def test_group_deletion_with_user_membership(self):
        # Create user user1
        user = Users(userid='user1',
                     first_name='user1_first',
                     last_name='user1_last')
        db.session.add(user)
        db.session.commit()

        # Create the group we need to fetch
        group = Groups(
                group_name='test_group',
                users=[user])
        db.session.add(group)
        db.session.commit()

        response = self.app.delete(
                '/groups/test_group',
                headers={"Content-Type": "application/json"})

        self.assertEqual(204, response.status_code)

        # We need to check if the cascase delete worked or not
        user1 = Users.query.get('user1')
        self.assertEqual([], user1.groups)

    def test_group_update_without_user_membership(self):
        # Creating the test user that we will update to the group
        user = Users(userid='user1',
                     first_name='user1_first',
                     last_name='user1_last')
        db.session.add(user)
        db.session.commit()

        # Create the group we need to update
        group = Groups(group_name='test_group')
        db.session.add(group)
        db.session.commit()

        # Updated group definition
        group_users = {
            'users': ['user1'],
        }

        response = self.app.put(
                '/groups/test_group',
                headers={"Content-Type": "application/json"},
                data=json.dumps(group_users))

        self.assertEqual(202, response.status_code)
        self.assertEqual(['user1'], response.json['users'])

    def test_user_update_with_group_membership(self):
        # Create user "user1"
        user1 = Users(
                userid='user1',
                first_name='user1_first',
                last_name='user1_last')
        db.session.add(user1)
        db.session.commit()

        # Create group test_group with user1 as member
        group = Groups(group_name='test_group', users=[user1])
        db.session.add(group)
        db.session.commit()

        # Create user2 that we will add to the lists of users for the
        # test_group
        user2 = Users(
                userid='user2',
                first_name='user2_first',
                last_name='user2_last')
        db.session.add(user2)
        db.session.commit()

        # Updated group membership to add user2 as well
        group_users = {
            'users': ['user1', 'user2'],
        }

        response = self.app.put(
                '/groups/test_group',
                headers={"Content-Type": "application/json"},
                data=json.dumps(group_users))

        self.assertEqual(202, response.status_code)
        self.assertEqual('test_group', response.json['group_name'])
        self.assertEqual(['user1', 'user2'], response.json['users'])

        # We need to check if the user2's groups were updated
        #user2 = Users.query.get('user2')
        #self.assertEqual([], user2.groups)

        # Update group to membership to remove all users.
        group_users = {
            'users': [],
        }

        response = self.app.put(
                '/groups/test_group',
                headers={"Content-Type": "application/json"},
                data=json.dumps(group_users))

        self.assertEqual(202, response.status_code)
        self.assertEqual('test_group', response.json['group_name'])
        self.assertEqual([], response.json['users'])
