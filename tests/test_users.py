import unittest
import json
from app import app, Users, Groups
from app import db


class UserTests(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        db.drop_all()
        self.db = db.create_all()

    def tearDown(self):
        db.drop_all()

    def test_user_creation(self):

        user = {
            'userid': 'test1_userid',
            'first_name': 'test1_first_name',
            'last_name': 'test1_last_name'
        }

        response = self.app.post('/users',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(user))

        self.assertEqual(str, type(response.json['userid']))
        self.assertEqual(200, response.status_code)

    def test_user_creation_with_malformed_json(self):

        user = {
            'userid': 'test1_userid',
            'my_name': 'test1_first_name',
            'last_name': 'test1_last_name'
        }

        response = self.app.post('/users',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(user))

        self.assertEqual(400, response.status_code)

    def test_user_creation_with_nonexisting_group(self):
        user = {
            'userid': 'test1_userid',
            'first_name': 'test1_first_name',
            'last_name': 'test1_last_name',
            'groups': ["admin"],
        }

        response = self.app.post('/users',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(user))

        self.assertEqual(404, response.status_code)

    def test_user_creation_with_existing_group(self):
        # Create group "admin"
        new_group = Groups(group_name='admin')
        db.session.add(new_group)
        db.session.commit()

        # Create Test user
        user = {
            'userid': 'test1_userid',
            'first_name': 'test1_first_name',
            'last_name': 'test1_last_name',
            'groups': ["admin"],
        }

        response = self.app.post('/users',
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(user))

        self.assertEqual(200, response.status_code)

    def test_fetch_user_without_group_membership(self):
        # Create the user we need to fetch
        new_user = Users(
                userid='test1_userid',
                first_name='test1_first_name',
                last_name='test1_last_name')
        db.session.add(new_user)
        db.session.commit()

        response = self.app.get(
                '/users/test1_userid',
                headers={"Content-Type": "application/json"})

        self.assertEqual('test1_userid', response.json['userid'])
        self.assertEqual(200, response.status_code)

    def test_fetch_user_with_group_membership(self):
        # Create group "admin"
        new_group = Groups(group_name='admin')
        db.session.add(new_group)
        db.session.commit()

        # Create the user we need to fetch
        new_user = Users(
                userid='test1_userid',
                first_name='test1_first_name',
                last_name='test1_last_name',
                groups=[new_group])
        db.session.add(new_user)
        db.session.commit()

        response = self.app.get(
                '/users/test1_userid',
                headers={"Content-Type": "application/json"})

        self.assertEqual('test1_userid', response.json['userid'])
        self.assertEqual('admin', response.json['groups'][0])
        self.assertEqual(200, response.status_code)

    def test_user_deletion_without_group_membership(self):

        # Create the user we need to delete
        new_user = Users(
                userid='test1_userid',
                first_name='test1_first_name',
                last_name='test1_last_name')
        db.session.add(new_user)
        db.session.commit()

        response = self.app.delete(
                '/users/test1_userid',
                headers={"Content-Type": "application/json"})

        self.assertEqual(204, response.status_code)

    def test_user_deletion_with_group_membership(self):
        # Create group "admin"
        new_group = Groups(group_name='admin')
        db.session.add(new_group)
        db.session.commit()

        # Create the user we need to delete
        new_user = Users(
                userid='test1_userid',
                first_name='test1_first_name',
                last_name='test1_last_name',
                groups=[new_group])
        db.session.add(new_user)
        db.session.commit()

        response = self.app.delete(
                '/users/test1_userid',
                headers={"Content-Type": "application/json"})

        self.assertEqual(204, response.status_code)

        # We need to check if the cascase delete worked or not
        admin_group = Groups.query.get('admin')
        self.assertEqual([], admin_group.users)

    def test_user_update_without_group_membership(self):
        # Create the user we need to update
        new_user = Users(
                userid='test1_userid',
                first_name='test1_first_name',
                last_name='test1_last_name')
        db.session.add(new_user)
        db.session.commit()

        # Updated  user definition
        user = {
            'userid': 'new_userid',
            'first_name': 'new_first',
            'last_name': 'new_last',
        }

        response = self.app.put(
                '/users/test1_userid',
                headers={"Content-Type": "application/json"},
                data=json.dumps(user))

        self.assertEqual(202, response.status_code)
        self.assertEqual('new_userid', response.json['userid'])
        self.assertEqual('new_first', response.json['first_name'])
        self.assertEqual('new_last', response.json['last_name'])

    def test_user_update_with_group_membership(self):
        # Create group "admin"
        new_group = Groups(group_name='admin')
        db.session.add(new_group)
        db.session.commit()

        # Create the user we need to update
        new_user = Users(
                userid='test1_userid',
                first_name='test1_first_name',
                last_name='test1_last_name',
                groups=[new_group])
        db.session.add(new_user)
        db.session.commit()

        # Updated  user definition
        user = {
            'userid': 'new_userid',
            'first_name': 'new_first',
            'last_name': 'new_last',
            'groups': ["ops"],
        }

        new_group = Groups(group_name='ops')
        db.session.add(new_group)
        db.session.commit()

        response = self.app.put(
                '/users/test1_userid',
                headers={"Content-Type": "application/json"},
                data=json.dumps(user))

        self.assertEqual(202, response.status_code)
        self.assertEqual('new_userid', response.json['userid'])
        self.assertEqual('new_first', response.json['first_name'])
        self.assertEqual('new_last', response.json['last_name'])
        self.assertEqual('ops', response.json['groups'][0])

        # We need to check if the user was removed from "admin" group
        admin_group = Groups.query.get('admin')
        self.assertEqual([], admin_group.users)
