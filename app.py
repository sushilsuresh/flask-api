from flask import Flask
from flask_restful import Api, Resource, request
from flask_sqlalchemy import SQLAlchemy
# from flask_marshmallow import Marshmallow
from sqlalchemy import exc
from resources.errors import errors
from resources.errors import InternalServerError, UserDoesNotExistError, \
        GroupDoesNotExistError
import json

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///people.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
api = Api(app, errors=errors)

membership = db.Table('membership',
                      db.Column('userid', db.String,
                                db.ForeignKey('users.userid'),
                                primary_key=True),
                      db.Column('group_name', db.String,
                                db.ForeignKey('groups.group_name'),
                                primary_key=True))


class Users(db.Model):
    userid = db.Column(db.String(25), nullable=False, primary_key=True)
    first_name = db.Column(db.String(255), nullable=False)
    last_name = db.Column(db.String(255), nullable=False)
    groups = db.relationship('Groups', secondary=membership,
                             back_populates='users')

    def __repr__(self):
        return '<User %s>' % self.userid


class Groups(db.Model):
    group_name = db.Column(db.String(25), nullable=False, primary_key=True)
    users = db.relationship('Users', secondary=membership,
                            back_populates='groups')

    def __repr__(self):
        return '<Group %s>' % self.group_name


def user_schema(user_obj):
    user = {}
    user['userid'] = user_obj.userid
    user['first_name'] = user_obj.first_name
    user['last_name'] = user_obj.last_name
    user['groups'] = []
    for group in user_obj.groups:
        user['groups'].append(group.group_name)
    return user


def group_schema(group_obj):
    group = {}
    group['group_name'] = group_obj.group_name
    group['users'] = []
    for user in group_obj.users:
        group['users'].append(user.userid)
    return group


class user(Resource):
    def get(self, userid):
        try:
            user = Users.query.get(userid)
        except Exception:
            return errors['InternalServerError'], \
                   errors['InternalServerError']['status']
        if user:
            return user_schema(user)
        else:
            return errors['UserDoesNotExistError'], \
                   errors['UserDoesNotExistError']['status']

    def put(self, userid):
        try:
            user = Users.query.get(userid)
            if not user:
                raise UserDoesNotExistError
            groups = set()
            for group_name in request.json.get('groups', []):
                group = Groups.query.get(group_name)
                if not group:
                    raise GroupDoesNotExistError
                groups.add(group)
            user.userid = request.json['userid']
            user.first_name = request.json['first_name']
            user.last_name = request.json['last_name']
            user.groups = list(groups)
            db.session.add(user)
            db.session.commit()
        except KeyError:
            # need to figure out exception flow
            # raise SchemaValidationError
            return errors['SchemaValidationError'], \
                   errors['SchemaValidationError']['status']
        except UserDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['UserAlreadyExistsError'], \
                   errors['UserAlreadyExistsError']['status']
        except GroupDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['GroupDoesNotExistError'], \
                   errors['GroupDoesNotExistError']['status']
        except Exception:
            return errors['InternalServerError'], \
                   errors['InternalServerError']['status']
        return user_schema(user), 202

    def delete(self, userid):
        try:
            user = Users.query.get(userid)
            if not user:
                raise UserDoesNotExistError
            db.session.delete(user)
            db.session.commit()
        except UserDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['UserDoesNotExistError'], \
                   errors['UserDoesNotExistError']['status']
        except Exception:
            return errors['InternalServerError'], \
                   errors['InternalServerError']['status']
        return '', 204


class user_create(Resource):
    def post(self):
        try:
            groups = set()
            for group_name in request.json.get('groups', []):
                group = Groups.query.get(group_name)
                if not group:
                    raise GroupDoesNotExistError
                groups.add(group)
            new_user = Users(
                userid=request.json['userid'],
                first_name=request.json['first_name'],
                last_name=request.json['last_name'],
                groups=list(groups)
            )
            db.session.add(new_user)
            db.session.commit()
        except KeyError:
            # need to figure out exception flow
            # raise SchemaValidationError
            return errors['SchemaValidationError'], \
                   errors['SchemaValidationError']['status']
        except exc.IntegrityError:
            # need to figure out exception flow
            # raise UserAlreadyExistsError
            return errors['UserAlreadyExistsError'], \
                   errors['UserAlreadyExistsError']['status']
        except GroupDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['GroupDoesNotExistError'], \
                   errors['GroupDoesNotExistError']['status']
        except Exception:
            return errors['InternalServerError'], \
                   errors['InternalServerError']['status']
        return user_schema(new_user)


class group(Resource):
    def get(self, group_name):
        try:
            group = Groups.query.get(group_name)
            if group:
                return group_schema(group)
            else:
                raise GroupDoesNotExistError
        except GroupDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['GroupDoesNotExistError'], \
                   errors['GroupDoesNotExistError']['status']
        except Exception:
            return errors['InternalServerError'], \
                   errors['InternalServerError']['status']

    def put(self, group_name):
        try:
            group = Groups.query.get(group_name)
            if not group:
                raise GroupDoesNotExistError
            users = set()
            for userid in request.json['users']:
                user = Users.query.get(userid)
                if not user:
                    raise UserDoesNotExistError
                users.add(user)
            group.users = list(users)
            db.session.add(group)
            db.session.commit()
        except KeyError:
            # need to figure out exception flow
            # raise SchemaValidationError
            return errors['SchemaValidationError'], \
                   errors['SchemaValidationError']['status']
        except UserDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['UserDoesNotExistsError'], \
                   errors['UserDoesNotExistsError']['status']
        except GroupDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['GroupDoesNotExistError'], \
                   errors['GroupDoesNotExistError']['status']
        except Exception:
            return errors['InternalServerError'], \
                   errors['InternalServerError']['status']
        return group_schema(group), 202

    def delete(self, group_name):
        try:
            group = Groups.query.get_or_404(group_name)
            if not group:
                raise GroupDoesNotExistError
            db.session.delete(group)
            db.session.commit()
        except GroupDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['GroupDoesNotExistError'], \
                   errors['GroupDoesNotExistError']['status']
        except Exception:
            return errors['InternalServerError'], \
                   errors['InternalServerError']['status']
        return '', 204


class group_create(Resource):
    def post(self):
        try:
            users = set()
            for userid in request.json.get('users', []):
                user = Users.query.get(userid)
                if not user:
                    raise UserDoesNotExistError
                users.add(user)
            new_group = Groups(group_name=request.json['group_name'],
                               users=list(users))
            db.session.add(new_group)
            db.session.commit()
        except KeyError:
            # need to figure out exception flow
            # raise SchemaValidationError
            return errors['SchemaValidationError'], \
                   errors['SchemaValidationError']['status']
        except exc.IntegrityError:
            # need to figure out exception flow
            # raise GroupAlreadyExistsError
            return errors['GroupAlreadyExistsError'], \
                   errors['GroupAlreadyExistsError']['status']
        except UserDoesNotExistError:
            # need to figure out exception flow
            # raise
            return errors['UserDoesNotExistError'], \
                   errors['UserDoesNotExistError']['status']
        except Exception:
            return errors['InternalServerError'], \
                   errors['InternalServerError']['status']
        return group_schema(new_group)


api.add_resource(user, '/users/<string:userid>')
api.add_resource(user_create, '/users')
api.add_resource(group, '/groups/<string:group_name>')
api.add_resource(group_create, '/groups')


if __name__ == '__main__':
    app.run(debug=False)
