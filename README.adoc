== Flask app to test REST api

=== TLDR
[source, bash]
----
git clone https://gitlab.com/sushilsuresh/flask-api.git
cd flask-api
./setup.sh
----

=== Assumptions

. You have `python3` installed and `python3` is your primary `python`
. `virtualenv` is installed and available to create python virtual environments

=== Running the Application
Please feel free to go through the `setup.sh` script. The tasks performed by
`setup.sh` are

* Create a python3 virtualenv
* Install required python modules in the virtual env
* Run python unit tests to make sure all is working well
* initialise the sqlite3 database `people.db`
* Start the flask app.

If you need to run the application at a later time without runnig the
`setup.sh` just active the virtual env and run `python app.py`

==== Starting the app manually after setup.
[source, bash]
----
sushil@ubuntu:~/flask-api$ source venv/bin/activate
(venv) sushil@ubuntu:~/flask-api$ python app.py
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
127.0.0.1 - - [17/Aug/2020 04:59:35] "POST /users HTTP/1.1" 200 -
127.0.0.1 - - [17/Aug/2020 04:59:35] "POST /groups HTTP/1.1" 200 -
127.0.0.1 - - [17/Aug/2020 04:59:35] "PUT /groups/admin HTTP/1.1" 202 -
127.0.0.1 - - [17/Aug/2020 04:59:35] "POST /groups HTTP/1.1" 200 -
127.0.0.1 - - [17/Aug/2020 04:59:35] "POST /users HTTP/1.1" 200 -
127.0.0.1 - - [17/Aug/2020 04:59:35] "PUT /users/jsmith HTTP/1.1" 202 -
127.0.0.1 - - [17/Aug/2020 04:59:35] "DELETE /groups/dev HTTP/1.1" 204 -
127.0.0.1 - - [17/Aug/2020 04:59:39] "DELETE /users/jdoe HTTP/1.1" 204 -
----

==== Making API calls
[source, bash]
----
sushil@ubuntu:~$ curl -X POST localhost:5000/users -H "Content-Type: application/json" -d '{"first_name":"Joe", "last_name":"Smith", "userid": "jsmith"}'
{"userid": "jsmith", "first_name": "Joe", "last_name": "Smith", "groups": []}
sushil@ubuntu:~$ curl -X POST localhost:5000/groups -H "Content-Type: application/json" -d '{"group_name":"admin"}'
{"group_name": "admin", "users": []}
sushil@ubuntu:~$ curl -X PUT localhost:5000/groups/admin -H "Content-Type: application/json" -d '{"users":["jsmith"]}'
{"group_name": "admin", "users": ["jsmith"]}
sushil@ubuntu:~$ curl -X POST localhost:5000/groups -H "Content-Type: application/json" -d '{"group_name":"dev"}'
{"group_name": "dev", "users": []}
sushil@ubuntu:~$ curl -X POST localhost:5000/users -H "Content-Type: application/json" -d '{"first_name":"John", "last_name":"Doe", "userid": "jdoe", "groups": ["admin"]}'
{"userid": "jdoe", "first_name": "John", "last_name": "Doe", "groups": ["admin"]}
sushil@ubuntu:~$ curl -X PUT localhost:5000/users/jsmith -H "Content-Type: application/json" -d '{"first_name":"Joe", "last_name":"Smith", "userid": "jsmith", "groups": ["admin", "dev"]}'
{"userid": "jsmith", "first_name": "Joe", "last_name": "Smith", "groups": ["admin", "dev"]}
sushil@ubuntu:~$ curl -X DELETE localhost:5000/groups/dev
sushil@ubuntu:~$ curl -X DELETE localhost:5000/users/jdoe
----

=== Running the funtional tests
The tests do not cover every use case, but gives you an idea. The functional
tests can be run using the commands below.

`python -m unittest discover ./tests/ -v`

==== Sample output
[source, bash]
----
$ source venv/bin/activate
(venv) $ python -m unittest discover ./tests/ -v
test_fetch_group_with_user_membership (test_groups.GroupTest) ... ok
test_fetch_group_without_user_membership (test_groups.GroupTest) ... ok
test_group_creation (test_groups.GroupTest) ... ok
test_group_creation_with_existing_user (test_groups.GroupTest) ... ok
test_group_creation_with_malformed_json (test_groups.GroupTest) ... ok
test_group_creation_with_nonexisting_users (test_groups.GroupTest) ... ok
test_group_deletion_with_user_membership (test_groups.GroupTest) ... ok
test_group_deletion_without_user_membership (test_groups.GroupTest) ... ok
test_group_update_without_user_membership (test_groups.GroupTest) ... ok
test_user_update_with_group_membership (test_groups.GroupTest) ... ok
test_fetch_user_with_group_membership (test_users.UserTests) ... ok
test_fetch_user_without_group_membership (test_users.UserTests) ... ok
test_user_creation (test_users.UserTests) ... ok
test_user_creation_with_existing_group (test_users.UserTests) ... ok
test_user_creation_with_malformed_json (test_users.UserTests) ... ok
test_user_creation_with_nonexisting_group (test_users.UserTests) ... ok
test_user_deletion_with_group_membership (test_users.UserTests) ... ok
test_user_deletion_without_group_membership (test_users.UserTests) ... ok
test_user_update_with_group_membership (test_users.UserTests) ... ok
test_user_update_without_group_membership (test_users.UserTests) ... ok

----------------------------------------------------------------------
Ran 20 tests in 0.397s

OK
----

=== Sample output from running setup.sh

[source, bash]
----
Setting up virtual env

created virtual environment CPython3.8.2.final.0-64 in 110ms
  creator CPython3Posix(dest=/home/sushil/test/flask-api/venv, clear=False, global=False)
  seeder FromAppData(download=False, webencodings=latest, pep517=latest, msgpack=latest, html5lib=latest, idna=latest, retrying=latest, ipaddr=latest, wheel=latest, distro=latest, colorama=latest, requests=latest, pip=latest, six=latest, CacheControl=latest, chardet=latest, distlib=latest, lockfile=latest, pytoml=latest, progress=latest, pkg_resources=latest, packaging=latest, contextlib2=latest, setuptools=latest, pyparsing=latest, urllib3=latest, certifi=latest, appdirs=latest, via=copy, app_data_dir=/home/sushil/.local/share/virtualenv/seed-app-data/v1.0.1.debian)
  activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator

Install necessary python modules

Collecting flask
  Using cached Flask-1.1.2-py2.py3-none-any.whl (94 kB)
Collecting flask_restful
  Using cached Flask_RESTful-0.3.8-py2.py3-none-any.whl (25 kB)
Collecting flask-marshmallow
  Using cached flask_marshmallow-0.13.0-py2.py3-none-any.whl (9.9 kB)
Collecting flask-SQLAlchemy
  Using cached Flask_SQLAlchemy-2.4.4-py2.py3-none-any.whl (17 kB)
Collecting marshmallow-sqlalchemy
  Using cached marshmallow_sqlalchemy-0.23.1-py2.py3-none-any.whl (18 kB)
Collecting click>=5.1
  Using cached click-7.1.2-py2.py3-none-any.whl (82 kB)
Collecting Jinja2>=2.10.1
  Using cached Jinja2-2.11.2-py2.py3-none-any.whl (125 kB)
Collecting itsdangerous>=0.24
  Using cached itsdangerous-1.1.0-py2.py3-none-any.whl (16 kB)
Collecting Werkzeug>=0.15
  Using cached Werkzeug-1.0.1-py2.py3-none-any.whl (298 kB)
Requirement already satisfied: six>=1.3.0 in ./venv/lib/python3.8/site-packages (from flask_restful->-r requirements.txt (line 2)) (1.14.0)
Collecting pytz
  Using cached pytz-2020.1-py2.py3-none-any.whl (510 kB)
Collecting aniso8601>=0.82
  Using cached aniso8601-8.0.0-py2.py3-none-any.whl (43 kB)
Collecting marshmallow>=2.0.0
  Using cached marshmallow-3.7.1-py2.py3-none-any.whl (45 kB)
Collecting SQLAlchemy>=0.8.0
  Using cached SQLAlchemy-1.3.18-cp38-cp38-manylinux2010_x86_64.whl (1.3 MB)
Collecting MarkupSafe>=0.23
  Using cached MarkupSafe-1.1.1-cp38-cp38-manylinux1_x86_64.whl (32 kB)
Installing collected packages: click, MarkupSafe, Jinja2, itsdangerous, Werkzeug, flask, pytz, aniso8601, flask-restful, marshmallow, flask-marshmallow, SQLAlchemy, flask-SQLAlchemy, marshmallow-sqlalchemy
Successfully installed Jinja2-2.11.2 MarkupSafe-1.1.1 SQLAlchemy-1.3.18 Werkzeug-1.0.1 aniso8601-8.0.0 click-7.1.2 flask-1.1.2 flask-SQLAlchemy-2.4.4 flask-marshmallow-0.13.0 flask-restful-0.3.8 itsdangerous-1.1.0 marshmallow-3.7.1 marshmallow-sqlalchemy-0.23.1 pytz-2020.1

	 Running Tests.

test_fetch_group_with_user_membership (test_groups.GroupTest) ... ok
test_fetch_group_without_user_membership (test_groups.GroupTest) ... ok
test_group_creation (test_groups.GroupTest) ... ok
test_group_creation_with_existing_user (test_groups.GroupTest) ... ok
test_group_creation_with_malformed_json (test_groups.GroupTest) ... ok
test_group_creation_with_nonexisting_users (test_groups.GroupTest) ... ok
test_group_deletion_with_user_membership (test_groups.GroupTest) ... ok
test_group_deletion_without_user_membership (test_groups.GroupTest) ... ok
test_group_update_without_user_membership (test_groups.GroupTest) ... ok
test_user_update_with_group_membership (test_groups.GroupTest) ... ok
test_fetch_user_with_group_membership (test_users.UserTests) ... ok
test_fetch_user_without_group_membership (test_users.UserTests) ... ok
test_user_creation (test_users.UserTests) ... ok
test_user_creation_with_existing_group (test_users.UserTests) ... ok
test_user_creation_with_malformed_json (test_users.UserTests) ... ok
test_user_creation_with_nonexisting_group (test_users.UserTests) ... ok
test_user_deletion_with_group_membership (test_users.UserTests) ... ok
test_user_deletion_without_group_membership (test_users.UserTests) ... ok
test_user_update_with_group_membership (test_users.UserTests) ... ok
test_user_update_without_group_membership (test_users.UserTests) ... ok

----------------------------------------------------------------------
Ran 20 tests in 0.429s

OK

Create sqlite3 database people.db

Example curl commands to test REST API

curl -X POST localhost:5000/users -H "Content-Type: application/json" -d '{"first_name":"Joe", "last_name":"Smith", "userid": "jsmith"}'
curl -X POST localhost:5000/groups -H "Content-Type: application/json" -d '{"group_name":"admin"}'
curl -X PUT localhost:5000/groups/admin -H "Content-Type: application/json" -d '{"users":["jsmith"]}'
curl -X POST localhost:5000/groups -H "Content-Type: application/json" -d '{"group_name":"dev"}'
curl -X POST localhost:5000/users -H "Content-Type: application/json" -d '{"first_name":"John", "last_name":"Doe", "userid": "jdoe", "groups": ["admin"]}'
curl -X PUT localhost:5000/users/jsmith -H "Content-Type: application/json" -d '{"first_name":"Joe", "last_name":"Smith", "userid": "jsmith", "groups": ["admin", "dev"]}'
curl -X DELETE localhost:5000/groups/dev
curl -X DELETE localhost:5000/users/jdoe

Starting the app by running - 'python app.py'

Press CTRL+C to quit

 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
----
