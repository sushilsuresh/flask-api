
class InternalServerError(Exception):
    pass

class SchemaValidationError(Exception):
    pass

class UserAlreadyExistsError(Exception):
    pass

class UserDoesNotExistError(Exception):
    pass

class GroupAlreadyExistsError(Exception):
    pass

class GroupDoesNotExistError(Exception):
    pass


errors = {
    'InternalServerError': {
        'message': "Internal Server Error ¯\_(ツ)_/¯",
        'status': 500,
    },
    'SchemaValidationError': {
        'message': "Request is missing required fields.",
        'status': 400,
    },
    'UserAlreadyExistsError': {
        'message': "A user with that userid already exists.",
        'status': 409,
    },
    'UserDoesNotExistError': {
        'message': "A user with that userid does not exist.",
        'status': 404,
    },
    'GroupAlreadyExistsError': {
        'message': "A group with that group_name already exists.",
        'status': 409,
    },
    'GroupDoesNotExistError': {
        'message': "A group with that group_name does not exist.",
        'status': 404,
    }
}
